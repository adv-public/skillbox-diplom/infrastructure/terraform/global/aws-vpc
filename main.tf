terraform {
  required_version = ">= 1.0.0, < 2.0.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

## Configure the AWS Provider
provider "aws" {
  region = "eu-central-1"
}

# Find out what data centers are in the selected region
data "aws_availability_zones" "available" {
  state = "available"
}

######################################################################
## environment = "stage"

# Module      : VPC for stage
module "vpc-stage" {
    source = "../../../modules/terraform-aws-vpc"

    name        = "vpc-1"
    environment = "stage"
    label_order = ["name", "environment"]

    vpc_enabled = true
    cidr_block  = "10.1.0.0/16"

}

# Module      : PUBLIC SUBNET for stage
module "subnets-stage" {
  source = "../../../modules/terraform-aws-subnet"

  name        = "subnets-1"
  environment = "stage"
  label_order = ["name", "environment"]

  nat_gateway_enabled = true

  availability_zones              = local.availability_zone # ["eu-central-1a", "eu-central-1b", "eu-central-1c"]
  vpc_id                          = module.vpc-stage.vpc_id
  type                            = "public"
  igw_id                          = module.vpc-stage.igw_id
  cidr_block                      = module.vpc-stage.vpc_cidr_block
  ipv6_cidr_block                 = module.vpc-stage.ipv6_cidr_block
  assign_ipv6_address_on_creation = false
}

######################################################################
## environment = "prod"

# Module      : VPC for prod
module "vpc-prod" {
    source = "../../../modules/terraform-aws-vpc"

    name        = "vpc-2"
    environment = "prod"
    label_order = ["name", "environment"]

    vpc_enabled = true
    cidr_block  = "10.2.0.0/16"

}

# Module      : PUBLIC SUBNET for prod
module "subnets-prod" {
  source = "../../../modules/terraform-aws-subnet"

  name        = "subnets-2"
  environment = "prod"
  label_order = ["name", "environment"]

  nat_gateway_enabled = true

  availability_zones              = local.availability_zone # ["eu-central-1a", "eu-central-1b", "eu-central-1c"]
  vpc_id                          = module.vpc-prod.vpc_id
  type                            = "public"
  igw_id                          = module.vpc-prod.igw_id
  cidr_block                      = module.vpc-prod.vpc_cidr_block
  ipv6_cidr_block                 = module.vpc-prod.ipv6_cidr_block
  assign_ipv6_address_on_creation = false
}

## elastic ip for stage
resource "aws_eip" "eip-nlb-stage" {
  tags = merge({
      Name        = format("%s%s%s", "stage", "-", "nlb-eip")
      environment = "stage"
    })
}

## elastic ip for stage
resource "aws_eip" "eip-nlb-prod" {
  tags = merge({
      Name        = format("%s%s%s", "prod", "-", "nlb-eip")
      environment = "prod"
    })
}