output "availability_zones" {
  value       = local.availability_zone
  description = "AWS Availability Zones within the region configured in the provider."
}

## environment = "stage"
output "vpc_id-stage" {
  value       = module.vpc-stage.vpc_id
  description = "The ID of the VPC."
}

output "subnet_id-stage" {
  value       = module.subnets-stage.public_subnet_id
  description = "The ID of the subnet."
}

output "vpc_default_security_group_id-stage" {
  value       = module.vpc-stage.vpc_default_security_group_id
  description = "The ID of the security group created by default on VPC creation."
}

output "eip-stage" {
  value       = aws_eip.eip-nlb-stage
  description = "The EIP for NLB stage."
}

## environment = "prod"
output "vpc_id-prod" {
  value       = module.vpc-prod.vpc_id
  description = "The ID of the VPC."
}

output "subnet_id-prod" {
  value       = module.subnets-prod.public_subnet_id
  description = "The ID of the subnet."
}

output "vpc_default_security_group_id-prod" {
  value       = module.vpc-prod.vpc_default_security_group_id
  description = "The ID of the security group created by default on VPC creation."
}

output "eip-prod" {
  value       = aws_eip.eip-nlb-prod
  description = "The EIP for NLB prod."
}